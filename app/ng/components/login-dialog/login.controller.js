'use strict';

angular.module('myApp')
    .controller('LoginController', function ($scope, CurrentUserService, $mdDialog) {
        $scope.email = '';
        $scope.pwd = '';
        $scope.errorText = '';

        $scope.login = login;
        $scope.cancel = cancel;

        function login() {
            CurrentUserService.login($scope.email, $scope.password).then(function () {
                $mdDialog.hide();
            }, function (response) {
                if (response.status == 400 || response.status == 401) {
                    $scope.errorText = 'Wrong username or password.';
                } else {
                    $scope.errorText = 'An unknown error occurred. please try again later.';
                }
            });
        }

        function cancel() {
            $mdDialog.cancel();
        }
    });
