angular.module('myApp')
    .directive('createRequest', function () {
        return {
            restrict: 'A',
            templateUrl: 'components/create-request-component/create-request.html',
            controller: 'CreateRequestController'
        }
    });
