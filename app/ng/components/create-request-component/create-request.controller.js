angular.module('myApp')
    .controller('CreateRequestController', function ($scope, $mdDialog, $rootScope, Request, RequestTypes) {
        $scope.request = {};
        $scope.requestTypes = RequestTypes;

        $scope.request = new Request();
        $scope.request.date = new Date();
        $scope.errorText = '';
        $scope.requestFormShown = false;

        $scope.submitRequest = submitRequest;
        $scope.toggleRequestForm = toggleRequestForm;

        function submitRequest() {
            console.log("Submit Request clicked");
            $scope.request.$save().then(function(request) {
                $rootScope.$broadcast('requestCreated', request);
                $mdDialog.hide(request);

                $scope.request = new Request();
                $scope.request.date = new Date();
                $scope.errorText = '';
                $scope.requestForm.$setPristine();
                $scope.requestForm.$setUntouched();
            }, function (response) {
                if (response.status == 401) {
                    $scope.errorText = 'Please log in first.';
                }
                if (response.status == 400) {
                    $scope.errorText = 'An unknown error occured. please try again later.';
                }
                if (response.status == 500) {
                    $scope.errorText = '500: Some internal backend error, sorry...';
                }
            });
        }

        function toggleRequestForm() {
            if ($scope.requestFormShown) {
                $scope.requestFormShown = false;
            } else {
                $scope.requestFormShown = true;
            }
        }
    });

