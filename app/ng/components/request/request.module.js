'use strict';

/**
 * Abstract state for requests.
 */
angular.module('myApp.request', ['ngResource', 'ui.router', 'ngMaterial'])
    .config(function ($stateProvider) {
        $stateProvider
            .state('request', {
                abstract: true,
                url: '/request'
            })
            .state('request.list', {
                url: '',
                views: {
                    'content@': {
                        templateUrl: 'views/request/list/request-list.html',
                        controller: 'RequestListController'
                    }
                }
            })
            .state('request.detail', {
                url: '/:requestId',
                views: {
                    'content@': {
                        templateUrl: 'views/request/detail/request-detail.html',
                        controller: 'RequestDetailController'
                    }
                }
            })
    });
