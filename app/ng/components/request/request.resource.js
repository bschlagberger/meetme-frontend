'use strict';

angular.module('myApp.request')
    .factory('Request', function ($resource, $http, BASEURL) {
        var requestResource = $resource(BASEURL + '/api/requests/:requestId', {requestId: '@_id'});

        /**
         * Load the list of messages of the given request.
         */
        requestResource.getConversation = function (requestId) {
            return $http.get(BASEURL + '/api/messages/' + requestId);
        };

        /**
         * Send message to the creator or accepter of this request
         */
        requestResource.sendMessage = function (requestId, message) {
            return $http.post(BASEURL + '/api/messages/' + requestId, {
                content: message
            });
        };
        requestResource.getMyRequests = function() {
            return $http.get(BASEURL + '/api/requests/my_created');
        };
        requestResource.getMyAcceptedRequests = function() {
            return $http.get(BASEURL + '/api/requests/my_accepted');
        };
        requestResource.getRequestWithPartialTitle = function(partialName) {
            return $http.get(BASEURL + '/api/requests/_searchTitle/' + partialName);
        };
        requestResource.getRequestWithPartialDescription = function(partialName) {
            return $http.get(BASEURL + '/api/requests/_searchDescription/' + partialName);
        };
        return requestResource;
    });
