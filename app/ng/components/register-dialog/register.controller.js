'use strict';

angular.module('myApp')
    .controller('RegisterController', function ($scope, CurrentUserService, $mdDialog) {
        $scope.email = '';
        $scope.pwd = '';
        $scope.firstname = '';
        $scope.lastname = '';
        $scope.country = '';
        $scope.pwdConfirm
        $scope.errorText = '';

        $scope.register = register;
        $scope.cancel = cancel;

        function register() {
            CurrentUserService.register($scope.email, $scope.pwd, $scope.firstname, $scope.lastname, $scope.country)
                .then(function () {
                    $mdDialog.hide();
                }, function (response) {
                    if (response.status == 400 || response.status == 401) {
                        $scope.errorText = 'An unknown error occured. please try again later.';
                    }
                });
        }

        function cancel() {
            $mdDialog.cancel();
        }
    });
