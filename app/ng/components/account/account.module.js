'use strict';

/**
 * State showing the user account details.
 */
angular.module('myApp.account', [])
    .config(function ($stateProvider) {
        $stateProvider
            .state('account', {
                url: '/account',
                views: {
                    'content@': {
                        templateUrl: 'views/account/account.html',
                        controller: 'AccountController'
                    }
                }
            })
    });
