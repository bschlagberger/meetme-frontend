'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['myApp.request', 'myApp.offer', 'myApp.account', 'ui.router', 'ngResource', 'templates', 'ngMaterial', 'ngMessages'])
    .config(function($stateProvider, $urlRouterProvider, $resourceProvider, $httpProvider, $mdThemingProvider, $mdDateLocaleProvider) {
        // For any unmatched url, redirect to /
        $urlRouterProvider.otherwise('/home');

        $stateProvider
            .state('home', {
                url: '/home',
                views: {
                    'content': {
                        templateUrl: 'views/home/home.html',
                        controller: 'HomeController'
                    }
                }
            });

        // This overrides the defaults actions for all $resources
        angular.extend($resourceProvider.defaults.actions, {
            update: {
                method: 'PUT'
            }
        });

        $httpProvider.interceptors.push('ReqErrInterceptor');
        //auth interceptor
        $httpProvider.interceptors.push('AuthInterceptor');

        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('lime');

        $mdDateLocaleProvider.formatDate = function(date) {
            return date ? date.toLocaleDateString('de') : '';
        };

        $mdDateLocaleProvider.parseDate = function(dateString) {
            return new Date(dateString);
        };
    })
    .constant('RequestTypes', [
        'Pickup Service',
        'Education',
        'Organization',
        'General'
    ])
    .constant('OfferTypes', [
        'Trips',
        'Education',
        'Organization',
        'General'
    ]);
