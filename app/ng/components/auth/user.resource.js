'use strict';

angular.module('myApp')
    .factory('User', function ($resource, BASEURL) {
        return $resource(BASEURL + '/api/user/:userId', {userId: '@_id'});
    });
