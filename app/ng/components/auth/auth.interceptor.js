'use strict';

angular.module('myApp')
    .factory('AuthInterceptor', function (BASEURL, AuthenticationService) {
        function req(config) {
            // automatically attach Authorization header
            if (config.url.indexOf(BASEURL) === 0 && AuthenticationService.isAuthed()) {
                var token = AuthenticationService.getToken();
                config.headers.Authorization = 'JWT ' + token;
            }

            return config;
        }

        function res(res) {
            // If a token was sent back, save it
            if (res && res.config.url.indexOf(BASEURL) === 0 && res.data.token) {
                AuthenticationService.saveToken(res.data.token);
            }

            return res;
        }

        return {
            request: req,
            response: res
        };
    });
