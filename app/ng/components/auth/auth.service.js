'use strict';

angular.module('myApp')
    .service('AuthenticationService', function ($window) {
        var self = this;
        this.token = '';

        this.isAuthed = isAuthed;
        this.getUserId = getUserId;
        this.saveToken = saveToken;
        this.getToken = getToken;
        this.deleteToken = deleteToken;

        function saveToken(t) {
            $window.localStorage['jwtToken'] = t;
        }

        function getToken() {
            return $window.localStorage['jwtToken'];
        }

        function deleteToken() {
            $window.localStorage.removeItem('jwtToken');
        }

        function getUserId() {
            if (!isAuthed()) {
                return null;
            }
            var base64Url = self.getToken().split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse($window.atob(base64));
        }

        function isAuthed() {
            var token = self.getToken();
            return !!token;
        }

    });
