'use strict';

angular.module('myApp')
    .service('CurrentUserService', function (BASEURL, $http, $state, AuthenticationService, User) {
        this.register = register;
        this.login = login;
        this.loggedIn = AuthenticationService.isAuthed;
        this.logout = logout;
        this.getUser = getUser;

        function register(email, pass, firstname, lastname, country) {
            return $http.post(BASEURL + '/signup', {
                email: email,
                password: pass,
                firstname: firstname,
                lastname: lastname,
                country: country
            });
        }

        function login(email, pass) {
            return $http.post(BASEURL + '/login', {
                email: email,
                password: pass
            });
        }

        function logout() {
            AuthenticationService.deleteToken();
            $state.go('home');
        }

        function getUser() {
            var userId = AuthenticationService.getUserId();
            return userId ? User.get(userId) : {};
        }
    });
