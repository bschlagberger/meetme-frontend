'use strict';

angular.module('myApp')
    .directive('menuToolbar', function () {
        return {
            restrict: 'A',
            templateUrl: 'components/menu-toolbar/menu-toolbar.html',
            controller: 'MenuController'
        }
    });
