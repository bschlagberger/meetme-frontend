'use strict';

angular.module('myApp')
    .controller('MenuController', function ($scope, $mdSidenav) {
        $scope.toggleList = function () {
            $mdSidenav('left').toggle();
        }
    })

    .config(function ($mdIconProvider) {
        $mdIconProvider.icon("menu", "./img/menu_list.svg", 24)
    });
