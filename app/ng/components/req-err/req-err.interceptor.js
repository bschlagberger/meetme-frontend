(function () {
    angular.module('myApp')
        .factory('ReqErrInterceptor', ReqErrInterceptor);

    function ReqErrInterceptor($injector, $q, AuthenticationService) {
        return {
            responseError: responseError
        };

        function responseError(rej) {
            if (rej.status == -1) {
                showAlert({title: 'Connection Error', msg: 'Could not reach the server. Try again later'});
            }

            if (rej.status == 401) {
                // User is not logged in
                var $state = $injector.get('$state');
                $state.go('home');
                AuthenticationService.deleteToken();
                showAlert({title: 'Unauthorized', msg: 'Please log in first'});
            }

            return $q.reject(rej);
        }

        function showAlert(opt) {
            //inject manually to resolve circular dependency error
            var $mdDialog = $injector.get('$mdDialog');
            var alert = $mdDialog.alert({
                title: opt.title,
                textContent: opt.msg,
                ok: 'Close'
            });

            $mdDialog.show(alert);
        }
    }
})();
