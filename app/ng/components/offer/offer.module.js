'use strict';

/**
 * Abstract state for offers.
 */
angular.module('myApp.offer', ['ngResource', 'ui.router', 'ngMaterial'])
    .config(function ($stateProvider) {
        $stateProvider
            .state('offer', {
                abstract: true,
                url: '/offer'
            })
            .state('offer.list', {
                url: '',
                views: {
                    'content@': {
                        templateUrl: 'views/offer/list/offer-list.html',
                        controller: 'OfferListController'
                    }
                }
            })
            .state('offer.detail', {
                url: '/:offerId',
                views: {
                    'content@': {
                        templateUrl: 'views/offer/detail/offer-detail.html',
                        controller: 'OfferDetailController'
                    }
                }
            })
    });
