'use strict';

angular.module('myApp.offer')
    .factory('Offer', function ($resource, $http, BASEURL) {
        var offerResource = $resource(BASEURL + '/api/offers/:offerId', {offerId: '@_id'});

        /**
         * Load the list of messages of the given offer.
         */
        offerResource.getConversation = function (offerId) {
            return $http.get(BASEURL + '/api/messages/' + offerId);
        };

        /**
         * Send message to the creator or acceptors of this offer
         */
        offerResource.sendMessage = function (offerId, message) {
            return $http.post(BASEURL + '/api/messages/' + offerId, {
                content: message
            });
        };
    
        offerResource.getMyOffers = function() {
           return $http.get(BASEURL + '/api/offers/my_created');
        };
        offerResource.getMyAcceptedOffers = function() {
            return $http.get(BASEURL + '/api/offers/my_accepted');
        };
        
        return offerResource;
    });
