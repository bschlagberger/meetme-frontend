'use strict';

angular.module('myApp')
    .directive('accountSection', function() {
        return {
            restrict: 'A',
            templateUrl: 'components/account-section/account-section.html',
            controller: function($scope, CurrentUserService, $mdDialog, $mdMedia) {
                $scope.user = null;

                $scope.showLoginDialog = showLoginDialog;
                $scope.showSignupDialog = showSignupDialog;
                $scope.logout = logout;

                $scope.$watch(function(){
                    return CurrentUserService.loggedIn();
                }, function(loggedIn){
                    $scope.loggedIn = loggedIn;
                    if (loggedIn && !$scope.user) {
                        $scope.user = CurrentUserService.getUser();
                    }
                });

                function showLoginDialog(){
                    var useFullScreen = $mdMedia('xs');
                    $mdDialog.show({
                        controller: 'LoginController',
                        templateUrl: 'components/login-dialog/login-dialog.html',
                        clickOutsideToClose:true,
                        fullscreen: useFullScreen
                    });
                }
                
                function showSignupDialog(){
                    var useFullScreen = $mdMedia('xs');
                    $mdDialog.show({
                        controller: 'RegisterController',
                        templateUrl: 'components/register-dialog/register-dialog.html',
                        clickOutsideToClose:true,
                        fullscreen: useFullScreen
                    });
                }

                function logout(){
                    CurrentUserService.logout();
                }
            }
        }
    });
