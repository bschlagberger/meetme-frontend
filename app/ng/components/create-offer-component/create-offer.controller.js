angular.module('myApp')
    .controller('CreateOfferController', function ($scope, $mdDialog, $rootScope, Offer, OfferTypes) {
        $scope.offer = {};
        $scope.offerTypes = OfferTypes;

        $scope.offer = new Offer();
        $scope.offer.date = new Date();
        $scope.errorText = '';
        $scope.offerFormShown = false;

        $scope.submitOffer = submitOffer;
        $scope.toggleOfferForm = toggleOfferForm;

        function submitOffer() {
            $scope.offer.$save().then(function(offer) {
                $rootScope.$broadcast('offerCreated', offer);
                $mdDialog.hide(offer);

                $scope.offer = new Offer();
                $scope.offer.date = new Date();
                $scope.errorText = '';
                $scope.offerForm.$setPristine();
                $scope.offerForm.$setUntouched();
            }, function (response) {
                if (response.status == 401) {
                    $scope.errorText = 'Please log in first.';
                }
                if (response.status == 400) {
                    $scope.errorText = 'An unknown error occured. please try again later.';
                }
            });
        }

        function toggleOfferForm() {
            if ($scope.offerFormShown) {
                $scope.offerFormShown = false;
            } else {
                $scope.offerFormShown = true;
            }
        }
    });

