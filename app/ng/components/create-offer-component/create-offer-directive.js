angular.module('myApp')
    .directive('createOffer', function () {
        return {
            restrict: 'A',
            templateUrl: 'components/create-offer-component/create-offer.html',
            controller: 'CreateOfferController'
        }
    });
