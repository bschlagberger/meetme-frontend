'use strict';

angular.module('myApp.offer')
    .controller('OfferListController', function ($scope, $state, $mdToast, Offer, AuthenticationService, CurrentUserService) {
        $scope.offers = [];
        $scope.formatDate = formatDate;
        $scope.showAcceptButton = showAcceptButton;
        $scope.acceptOffer = acceptOffer;

        $scope.$on('offerCreated', function (ev, offer) {
            $scope.offers.unshift(offer);

            $mdToast.show(
                $mdToast.simple()
                    .textContent('Offer created.')
                    .position('bottom right')
                    .hideDelay(3000)
            );
        });

        refreshData();

        function refreshData() {
            $scope.offers = Offer.query();
        }

        function formatDate(date) {
            return date ? moment(date).format('DD.MM.YYYY') : '';
        }

        /**
         * Don't show Accept button if currentUser is the creator
         */
        function showAcceptButton(offer) {
            var currentUserId = AuthenticationService.getUserId();
            return !!(offer._creator._id != currentUserId
                      && offer.maxNumberOfParticipants > offer._accepters.length
                      && offer._accepters.indexOf(currentUserId) === -1);
        }

        function acceptOffer(offer) {
            CurrentUserService.getUser().$promise.then(function (currentUser) {
                if (offer.maxNumberOfParticipants > offer._accepters.length) {
                    offer._accepters.push(currentUser);
                }

                offer.$save().then(function (offer) {
                    $state.go('offer.detail', {offerId: offer._id});
                });
            });
        }

    });
