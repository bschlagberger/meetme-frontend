'use strict';

angular.module('myApp.offer')
    .controller('OfferDetailController', function ($scope, Offer, AuthenticationService, CurrentUserService, $stateParams) {
        $scope.myId = AuthenticationService.getUserId();
        $scope.me = null;
        $scope.offer = {};
        $scope.chatEnabled = false;
        $scope.messages = [];
        $scope.newMessage = '';
        $scope.haveAccepted = false;
        $scope.acceptAllowed = false;
        $scope.isMyOffer = false;

        $scope.isMine = isMine;
        $scope.sendMessage = sendMessage;
        $scope.accept = accept;
        $scope.update = update;

        $scope.$watch($scope.offer, function(offer) {
            if (offer && offer._accepters.indexOf(me) !== -1) {
                $scope.haveAccepted = true;
            }

            if (offer && offer._creator !== $scope.myId && offer.maxNumberOfParticipants !== offer._accepters.length) {
                $scope.acceptAllowed = true;
            }
        });

        refresh();

        function isMine(message) {
            return message._sender._id === this.myId;
        }

        function sendMessage() {
            if (this.newMessage) {
                Offer.sendMessage($stateParams.offerId, this.newMessage).then(function (response) {
                    $scope.messages.push(response.data);
                });
                this.newMessage = '';
            }
        }

        function accept() {
            $scope.offer._accepters.push($scope.myId);

            $scope.offer.$save().then(function (offer) {
                $scope.offer = offer;
            });
        }

        function update() {
            $scope.offer.$save().then(function(updated){
                $scope.offer = updated;
            });
        }

        function refresh() {
            CurrentUserService.getUser().$promise.then(function (currentUser) {
                $scope.me = currentUser;
            });

            Offer.get({offerId: $stateParams.offerId}, function (offer) {
                $scope.offer = offer;
                if (offer._accepters.length > 0 && ($scope.myId === offer._creator._id
                    || (offer._accepters.indexOf($scope.myId) !== -1))) {
                    $scope.chatEnabled = true;
                    loadMessages();
                }

                if (offer && offer._creator._id === $scope.myId) {
                    $scope.isMyOffer = true;
                }
            });
        }

        function loadMessages() {
            Offer.getConversation($stateParams.offerId).then(function (response) {
                $scope.messages = response.data;
            });
        }
    });
