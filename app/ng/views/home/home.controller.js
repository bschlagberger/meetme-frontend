'use strict';

angular.module('myApp')
    .controller('HomeController', function($scope, $mdDialog, $mdMedia) {
        $scope.showRequestDialog = showRequestDialog;

        function showRequestDialog(){
            var useFullScreen = $mdMedia('xs');
            $mdDialog.show({
                controller: 'CreateRequestController',
                templateUrl: 'components/create-request-component/create-request.html',
                clickOutsideToClose:true,
                fullscreen: useFullScreen
            });
        }
    });
