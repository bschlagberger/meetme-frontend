'use strict';

angular.module('myApp.account')
    .controller('AccountController', function ($scope, CurrentUserService, Request, Offer, $mdDialog) {
        $scope.user = null;
        $scope.myRequests = [];
        $scope.myAcceptedRequests = [];
        $scope.myOffers = [];
        $scope.myAcceptedOffers = [];

        $scope.update = update;
        $scope.deleteItem = deleteItem;

        refresh();

        /**
         * Sends the updated user details to the server.
         */
        function update() {
            if ($scope.user) {
                $scope.user.$save(function (response) {
                    // TODO
                });
            }
        }

        /**
         * Fetches the user's details from the server.
         */
        function refresh() {
            $scope.user = CurrentUserService.getUser();
            Request.getMyRequests().then(function (response) {
                $scope.myRequests = response.data
            });
            Request.getMyAcceptedRequests().then(function (response) {
                $scope.myAcceptedRequests = response.data
            });
            Offer.getMyOffers().then(function (response) {
                $scope.myOffers = response.data
            });
            Offer.getMyAcceptedOffers().then(function (response) {
                $scope.myAccOffers = response.data
            });
        }

        function deleteItem(item, ev) {
            var confirm = $mdDialog.confirm()
                .title('Confirm Deletion')
                .textContent('Would you really like to delete this item?')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                if (item.__t === 'Request') {
                    Request.delete({requestId: item._id});
                } else {
                    Offer.delete({offerId: item._id});
                }
                refresh();
            });
        }
    });


