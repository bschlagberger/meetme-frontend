'use strict';

angular.module('myApp.request')
    .controller('RequestDetailController', function ($scope, $mdToast, $mdDialog, $stateParams, Request, AuthenticationService) {
        $scope.myId = AuthenticationService.getUserId();
        $scope.request = null;
        $scope.chatEnabled = false;
        $scope.messages = [];
        $scope.newMessage = '';
        $scope.haveAccepted = false;
        $scope.acceptAllowed = false;
        $scope.isMyRequest = false;

        $scope.isMine = isMine;
        $scope.sendMessage = sendMessage;
        $scope.accept = accept;

        refresh();

        function isMine(message) {
            return message._sender._id === this.myId;
        }

        function sendMessage() {
            if (this.newMessage) {
                Request.sendMessage($stateParams.requestId, this.newMessage).then(function (response) {
                    $scope.messages.push(response.data);
                });
                this.newMessage = '';
            }
        }

        function accept() {
            $scope.request._accepter = $scope.myId;

            $scope.request.$save().then(function () {
                refresh();
            });

        }

        function refresh() {
            Request.get({requestId: $stateParams.requestId}, function (request) {
                $scope.request = request;
                if (request._accepter && ($scope.myId === request._creator._id
                    || (request._accepter && $scope.myId === request._accepter._id))) {
                    $scope.chatEnabled = true;
                    loadMessages();
                }

                if (request && request._creator._id === $scope.myId) {
                    $scope.isMyRequest = true;
                }

                if (request && request._accepter === $scope.myId) {
                    $scope.haveAccepted = true;
                }

                if (request && request._creator._id !== $scope.myId) {
                    $scope.acceptAllowed = true;
                }
            });
        }

        function loadMessages() {
            Request.getConversation($stateParams.requestId).then(function (response) {
                $scope.messages = response.data;
            });
        }
    });
