'use strict';

angular.module('myApp.request')
    .controller('RequestListController', function ($scope, $state, $mdToast, Request, User, AuthenticationService, CurrentUserService) {
        $scope.requests = [];
        $scope.filteredRequests = [];
        $scope.formatDate = formatDate;
        $scope.showAcceptButton = showAcceptButton;
        $scope.acceptRequest = acceptRequest;
        $scope.getTitlesFromDb = getTitlesFromDb;
        $scope.getDescriptionsFromDb = getDescriptionsFromDb;

        $scope.$on('requestCreated', function (ev, request) {
            $scope.requests.unshift(request);

            $mdToast.show(
                $mdToast.simple()
                    .textContent('Request created.')
                    .position('bottom right')
                    .hideDelay(3000)
            );
        });

        refreshData();

        function refreshData() {
            $scope.requests = Request.query();
            $scope.filteredRequests = $scope.requests;
        }

        function formatDate(date) {
            return date ? moment(date).format('DD.MM.YYYY') : '';
        }

        /**
         * Don't show Accept button if currentUser is the creator
         */
        function showAcceptButton(request) {
            return request._creator._id != AuthenticationService.getUserId();
        }

        function acceptRequest(request) {
            console.log("Accept Request clicked");
            CurrentUserService.getUser().$promise.then(function (currentUser) {
                request._accepter = currentUser._id;
                delete request._creator;

                request.$save().then(function (request) {
                    $state.go('request.detail', {requestId: request._id})
                });
            });
        }

        function getTitlesFromDb() {
            if ($scope.filteredRequests == null) {
                $scope.filteredRequests = $scope.requests;
                $scope.apply();
            }
            Request.getRequestWithPartialTitle($scope.searchInDbByTitle).then(function (response) {
                $scope.filteredRequests = response.data
            });
        }

        function getDescriptionsFromDb() {
            if ($scope.filteredRequests == null) {
                $scope.filteredRequests = $scope.requests;
                $scope.apply();
            }
            Request.getRequestWithPartialDescription($scope.searchInDbByDescription).then(function (response) {
                $scope.filteredRequests = response.data
            });
        }
    });
